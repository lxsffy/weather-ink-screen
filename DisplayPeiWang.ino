#define cg_y0 20
#define cg_y1 cg_y0+19
#define cg_y2 cg_y1+19
#define cg_x0 65
void display_peiwang()
{
  display.setPartialWindow(0, 0, display.width(), display.height()); //设置局部刷新窗口
  u8g2Fonts.setFontMode(1);          // 使用u8g2透明模式（这是默认设置）
  u8g2Fonts.setFontDirection(0);     // 从左到右（这是默认设置）
  u8g2Fonts.setForegroundColor(heise);  // 设置前景色
  u8g2Fonts.setBackgroundColor(baise);  // 设置背景色

  ESP.wdtFeed();//喂狗
  u8g2Fonts.setFont(chinese_city_gb2312);
  display.firstPage();
  do
  {
    u8g2Fonts.setCursor(cg_x0, cg_y0);
    u8g2Fonts.print("热点名称:");
    u8g2Fonts.setCursor(cg_x0 + 65, cg_y0);
    u8g2Fonts.print(ap_ssid);

    u8g2Fonts.setCursor(cg_x0, cg_y1);
    u8g2Fonts.print("热点密码:");
    u8g2Fonts.setCursor(cg_x0 + 65, cg_y1);
    u8g2Fonts.print(ap_password);

    u8g2Fonts.setCursor(cg_x0, cg_y2);
    u8g2Fonts.print("热点地址:");
    u8g2Fonts.setCursor(cg_x0 + 65, cg_y2);
    u8g2Fonts.print(WiFi.softAPIP());
  }
  while (display.nextPage());
}
