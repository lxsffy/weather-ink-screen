String sendHTML_main() //主页面,备用页面
{
  String webpage_html;
  webpage_html += "<!DOCTYPE html>";
  webpage_html += "<html lang='zh-CN'>";
  webpage_html += "<head>";
  webpage_html += "<title>天气墨水屏配置</title>";
  webpage_html += "<meta charset='utf-8'>";
  webpage_html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  webpage_html += "</head>";
  webpage_html += "<body>";

  webpage_html += "<div class='container'>";

  webpage_html += "<h2>WIFI配置 <small><span id='STA_name'> </span> <span id='STA_ip'> </span></small></h2>";
  webpage_html += "<form name='input' action='/' method='POST'>";
  webpage_html += "<div class='form-group'>";
  webpage_html += "<label for='ssid'>WIFI名称:</label>";
  webpage_html += "<input type='text' class='form-control' name='ssid' placeholder='请输入'>";
  webpage_html += "</div>";
  webpage_html += "<div class='form-group'>";
  webpage_html += "<label for='password'>WIFI密码:</label>";
  webpage_html += "<input type='text' class='form-control' name='password' placeholder='请输入'>";
  webpage_html += "</div>";
  webpage_html += "<button type='submit' class='btn btn-primary'>保存并连接</button>";
  webpage_html += "</form>";

  webpage_html += "<br>";
  webpage_html += "<hr/>";

  webpage_html += "<h2>天气配置 <small><span id='GET_city'> </span></small></h2>";
  webpage_html += "<form name='input' action='/weather' method='POST'>";
  webpage_html += "<div class='form-group'>";
  webpage_html += "<label for='WeatherKey'>天气Key:</label>";
  webpage_html += "<input type='text' class='form-control' name='WeatherKey' placeholder='请输入'>";
  webpage_html += "</div>";
  webpage_html += "<div class='form-group'>";
  webpage_html += "<label for='city'>城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市:</label>";
  webpage_html += "<input type='text' class='form-control' name='city' placeholder='中文或拼音或城市V3ID'>";
  webpage_html += "</div>";
  webpage_html += " <button type='submit' class='btn btn-primary'>保存</button>";
  webpage_html += "</form>";
  webpage_html += "<br><br>";
  
  webpage_html += "<a href='/Updata'><button type='button' class='btn btn-success' style='margin-right:0.5%;'>固件更新</button></a>";
  webpage_html += "<a href='/FileUpdata1'><button type='button' class='btn btn-success' style='margin-right:3%;'>上传文件</button></a>";
  webpage_html += "<a href='/RESET'><button type='button' class='btn btn-info'>重启</button></a>";
  
  webpage_html += "<br><br><br>";

  webpage_html += "<div class='row' style='text-align: center; background-color:#e9ecef; font-size:0.7rem;'>";
  webpage_html += "<div class='col'>天气服务由 <a href='https://www.seniverse.com/'>心知天气</a> 提供</div>";
  webpage_html += "<div class='col'>一言服务由 <a href='https://hitokoto.cn/'>hitokoto.cn</a> 提供</div>";
  webpage_html += "</div>";
  webpage_html += "<div class='row' style='text-align: center; background-color:#dee2e6; font-size:0.7rem;'>";
  webpage_html += "<div class='col'>Power esp8266</div>";
  webpage_html += "<div class='col'>By 甘草酸不酸</div>";
  webpage_html += " <div class='col'>软件版本：008</div>";
  webpage_html += "</div>";


  webpage_html += "</div>";

  webpage_html += "<script>";
  webpage_html += "setInterval(function() {getSTA_name();getSTA_ip();get_city();}, 1000);"; //1秒获取一次数据 WIFI名称和IP地址

  webpage_html += "function getSTA_name() ";
  webpage_html += "{";
  webpage_html += "var xhttp = new XMLHttpRequest();";
  webpage_html += "xhttp.onreadystatechange = function()";  //当前数据源的数据将要发生变化时
  webpage_html += "{";
  webpage_html += "if (this.readyState == 4 && this.status == 200)";
  webpage_html += "{";
  webpage_html += "document.getElementById('STA_name').innerHTML = this.responseText;";
  webpage_html += "}";
  webpage_html += "};";
  webpage_html += "xhttp.open('GET', 'ReadSTA_name', true);";
  webpage_html += "xhttp.send();";
  webpage_html += "}";

  webpage_html += "function getSTA_ip() ";
  webpage_html += "{";
  webpage_html += "var xhttp = new XMLHttpRequest();";
  webpage_html += "xhttp.onreadystatechange = function()";  //当前数据源的数据将要发生变化时
  webpage_html += "{";
  webpage_html += "if (this.readyState == 4 && this.status == 200)";
  webpage_html += "{";
  webpage_html += "document.getElementById('STA_ip').innerHTML = this.responseText;";
  webpage_html += "}";
  webpage_html += "};";
  webpage_html += "xhttp.open('GET', 'ReadSTA_ip', true);";
  webpage_html += "xhttp.send();";
  webpage_html += "}";

  webpage_html += "function get_city()";
  webpage_html += "{";
  webpage_html += "var xhttp = new XMLHttpRequest();";
  webpage_html += "xhttp.onreadystatechange = function()";  //当前数据源的数据将要发生变化时
  webpage_html += "{";
  webpage_html += "if (this.readyState == 4 && this.status == 200)";
  webpage_html += "{";
  webpage_html += "document.getElementById('GET_city').innerHTML = this.responseText;";
  webpage_html += "}";
  webpage_html += "};";
  webpage_html += "xhttp.open('GET', 'Read_city', true);";
  webpage_html += "xhttp.send();";
  webpage_html += "}";

  webpage_html += "</script>";
  webpage_html += "</body>";
  webpage_html += "</html>";

  return webpage_html;
}

String sendHTML_fileUpdata() //文件上传页面
{
  String webpage_html;
  webpage_html += "<!DOCTYPE html>";
  webpage_html += "<html lang='zh - CN'>";
  webpage_html += "<head>";

  webpage_html += "<meta charset='utf-8'>";
  webpage_html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  webpage_html += "<link rel='stylesheet' type='text/css' href='bootstrap.css'>";
  webpage_html += "<title>天气墨水屏配置</title>";
  webpage_html += "</head>";
  webpage_html += "<body>";

  webpage_html += "<br>";
  webpage_html += "<div class='row' style='text-align:center;'>";
  webpage_html += "<div class='col'>";
  webpage_html += "<h3>ESP8266 SPIFFS 文件上传</h3>";
  webpage_html += "<p>点击按钮选择需要上传的文件</p>";

  webpage_html += "<form method='POST' class='btn btn-info' enctype='multipart/form-data'>";
  webpage_html += "<input type='file' name='data'>";
  webpage_html += "<input class='btn btn-danger' type='submit' value='上传'>";
  webpage_html += "</form>";

  webpage_html += "</div>";
  webpage_html += "</div>";
  webpage_html += "<br><br><br>";
  webpage_html += "<div class='row' style='text-align:center;'>";
  webpage_html += "<div class='col'>";
  webpage_html += "<a href='/'>";
  webpage_html += "<button type='button' class='btn btn-success'>返回首页</button>";
  webpage_html += "</a>";
  webpage_html += " </div>";

  webpage_html += "<div class='col'> ";
  webpage_html += "<a href='/RESET'>";
  webpage_html += "<button type='button' class='btn btn-info'>重启</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";
  webpage_html += "</div>";

  webpage_html += "</body>";
  webpage_html += "</html>";
  return webpage_html;
}
String sendHTML_gujian_update() //固件更新页
{
  String webpage_html;
  webpage_html += "<!DOCTYPE html>";
  webpage_html += "<html lang='zh-CN'>";
  webpage_html += "<head>";

  webpage_html += "<meta charset='utf-8'>";
  webpage_html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  webpage_html += "<link rel='stylesheet' type='text/css' href='bootstrap.css'>";
  webpage_html += "<title>天气墨水屏配置</title>";
  webpage_html += "</head>";
  webpage_html += "<body>";

  webpage_html += "<br>";
  webpage_html += "<div class='row' style='text-align:center;'>";
  webpage_html += "<div class='col'>";
  webpage_html += "<h3>WebServer更新固件模式</h3>";

  webpage_html += "<div class='col'>";
  webpage_html += "<div>http://" + WiFi.localIP().toString() + "/update</div>";
  webpage_html += "<a href=\"http://" + WiFi.localIP().toString() + "/update\">";
  webpage_html += "<button type='button' class='btn btn-danger'>点击进入更新页面(STA模式)</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";

  webpage_html += "<div class='col'>";
  webpage_html += "<div style='margin:15px 0px 0px 0px;'>http://" + WiFi.softAPIP().toString() + "/update</div>";
  webpage_html += "<a href=\"http://" + WiFi.softAPIP().toString() + "/update\">";
  webpage_html += "<button type='button' class='btn btn-danger'>点击进入更新页面(AP模式,未成功)</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";

  webpage_html += "</div>";
  webpage_html += "</div>";
  webpage_html += "<br><br><br>";
  webpage_html += "<div class='row' style='text-align:center;'>";
  webpage_html += "<div class='col'>";
  webpage_html += "<a href='/'>";
  webpage_html += "<button type='button' class='btn btn-success'>返回首页</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";

  webpage_html += "<div class='col'> ";
  webpage_html += "<a href='/RESET'>";
  webpage_html += "<button type='button' class='btn btn-info'>重启</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";
  webpage_html += "</div>";

  webpage_html += "</body>";
  webpage_html += "</html>";
  return webpage_html;
}
String sendHTML_return_home(String xiaoxi) //返回首页
{
  String webpage_html;
  webpage_html += "<!DOCTYPE html>";
  webpage_html += "<html lang='zh-CN'>";
  webpage_html += "<head>";

  webpage_html += "<meta charset='utf-8'>";
  webpage_html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  webpage_html += "<link rel='stylesheet' type='text/css' href='bootstrap.css'>";
  webpage_html += "<title>天气墨水屏配置</title>";
  webpage_html += "</head>";
  webpage_html += "<body>";

  webpage_html += "<br>";
  webpage_html += "<div class='row' style='text-align:center;'>";
  webpage_html += "<div class='col'>";
  webpage_html += "<h3>" + xiaoxi + "</h3>";
  webpage_html += "</div>";
  webpage_html += "</div>";

  webpage_html += "<div class='row' style='text-align:center;'>";
  webpage_html += "<div class='col'>";
  webpage_html += "<a href='/'>";
  webpage_html += "<button type='button' class='btn btn-success'>返回首页</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";

  webpage_html += "<div class='col'> ";
  webpage_html += "<a href='/RESET'>";
  webpage_html += "<button type='button' class='btn btn-info'>重启</button>";
  webpage_html += "</a>";
  webpage_html += "</div>";
  webpage_html += "</div>";

  webpage_html += "</body>";
  webpage_html += "</html>";
  return webpage_html;
}
