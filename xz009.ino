//测试版，不喜勿用，可用160Mhz运行
//开机时使用下降沿触发GPIO5可进入配网模式，具体热点信息看屏幕提示（原版硬件按下按键3）
//已知BUG1，在配网页面连接无效的的WIFI会卡一段时间，大概10S左右，就大概10S没有连接上wifi就关闭STA模式
//已知BUG2，开机载入数据有小几率会重启系统,原因未知，堆栈溢出？
//已知BUG3，配网界面更换城市无法即时更新天气数据

//基类GxEPD2\u GFX可以用来将引用或指针作为参数传递给display实例，使用了大约1.2k的代码
//启用或禁用GxEPD2\u GFX基类
#define ENABLE_GxEPD2_GFX 1
#include <GxEPD2_BW.h>
#include <U8g2_for_Adafruit_GFX.h>
GxEPD2_BW<GxEPD2_290, GxEPD2_290::HEIGHT> display(GxEPD2_290(/*CS=D8*/ SS, /*DC=D3*/ 0, /*RST=D4*/ 2, /*BUSY=D2*/ 4));
//296*128
U8G2_FOR_ADAFRUIT_GFX u8g2Fonts;

#define baise  GxEPD_WHITE  //白色
#define heise  GxEPD_BLACK  //黑色
//调用国内城市字库+GB2312
#include "gb2312.c"
//声明外部变量
extern const uint8_t chinese_city_gb2312[239032] U8G2_FONT_SECTION("chinese_city_gb2312");
//extern String css;
extern const unsigned char Bitmap_setup0[] PROGMEM;
extern const unsigned char Bitmap_setup1[] PROGMEM;
extern const unsigned char Bitmap_xztq[] PROGMEM;
extern const unsigned char Bitmap_wlq1[] PROGMEM;
extern const unsigned char Bitmap_wlq2[] PROGMEM;
extern const unsigned char Bitmap_wlq3[] PROGMEM;
extern const unsigned char Bitmap_wlq4[] PROGMEM;
extern const unsigned char Bitmap_qt[] PROGMEM;
extern const unsigned char Bitmap_dy[] PROGMEM;
extern const unsigned char Bitmap_yt[] PROGMEM;
extern const unsigned char Bitmap_zheny[] PROGMEM;
extern const unsigned char Bitmap_lzy[] PROGMEM;
extern const unsigned char Bitmap_lzybbb[] PROGMEM;
extern const unsigned char Bitmap_xy[] PROGMEM;
extern const unsigned char Bitmap_zhongy[] PROGMEM;
extern const unsigned char Bitmap_dayu[] PROGMEM;
extern const unsigned char Bitmap_by[] PROGMEM;
extern const unsigned char Bitmap_dby[] PROGMEM;
extern const unsigned char Bitmap_tdby[] PROGMEM;
extern const unsigned char Bitmap_dongy[] PROGMEM;
extern const unsigned char Bitmap_yjx[] PROGMEM;
extern const unsigned char Bitmap_zhenx[] PROGMEM;
extern const unsigned char Bitmap_xx[] PROGMEM;
extern const unsigned char Bitmap_zhongx[] PROGMEM;
extern const unsigned char Bitmap_dx[] PROGMEM;
extern const unsigned char Bitmap_bx[] PROGMEM;
extern const unsigned char Bitmap_fc[] PROGMEM;
extern const unsigned char Bitmap_ys[] PROGMEM;
extern const unsigned char Bitmap_scb[] PROGMEM;
extern const unsigned char Bitmap_w[] PROGMEM;
extern const unsigned char Bitmap_m[] PROGMEM;
extern const unsigned char Bitmap_f[] PROGMEM;
extern const unsigned char Bitmap_jf[] PROGMEM;
extern const unsigned char Bitmap_ljf[] PROGMEM;
extern const unsigned char Bitmap_wz[] PROGMEM;
extern const unsigned char Bitmap_qt_ws[] PROGMEM;
extern const unsigned char Bitmap_yt_wz[] PROGMEM;
extern const unsigned char Bitmap_dy_wz[] PROGMEM;
extern const unsigned char Bitmap_zy_wz[] PROGMEM;
extern const unsigned char Bitmap_zx_wz[] PROGMEM;
extern const unsigned char Bitmap_weizhi[] PROGMEM;
extern const unsigned char Bitmap_zhuangtai[] PROGMEM;
extern const unsigned char Bitmap_gengxing[] PROGMEM;
extern const unsigned char Bitmap_riqi[] PROGMEM;
extern const unsigned char Bitmap_batlow[] PROGMEM;

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP_EEPROM.h>  //根据官方库制作的升级版库
#include <Ticker.h>
#include <FS.h>
File fsUploadFile;              // 建立文件对象用于闪存文件上传
ESP8266WiFiMulti wifiMulti;     // 建立ESP8266WiFiMulti对象,对象名称是 'wifiMulti'
Ticker ticker;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "cn.ntp.org.cn", 8 * 3600, 6000); //udp，服务器地址，时间偏移量，更新间隔
//*** HTTP服务器配置
String language = "zh-Hans";              // 请求语言
String url_yiyan = "https://v1.hitokoto.cn/?encode=json&min_length=1&max_length=21";//一言获取地址
String url_ActualWeather; //实况天气地址
String url_FutureWeather; //未来天气地址
//****** WebServer设置 ******
ESP8266WebServer server(80); //建立网络服务器对象，该对象用于响应HTTP请求。监听端口（80）
ESP8266HTTPUpdateServer httpUpdater; //建立httpOTA对象
//****** STA设置
char sta_ssid[32] = {0};
char sta_password[64] = {0};
//char sta_ssid[32] = "Tenda_2B4C90";
//char sta_password[64] = "liwenjie1246753866";
//char sta_ssid[32] = "K20";
//char sta_password[64] = "326513988";
//char sta_ssid[32] = "Jones";
//char sta_password[64] = "326513988";
//****** AP设置
const char* ap_ssid = "ESP8266 E-Paper";
const char* ap_password = "19980902"; //无密码则为开放式网络
IPAddress local_IP(192, 168, 9, 2);
IPAddress gateway(192, 168, 9, 2);
IPAddress subnet(255, 255, 255, 0);
//****** 天气数据
//我们要从此网页中提取的数据的类型
struct ActualWeather
{
  char status_code[64]; //错误代码
  char city[16];//城市名称
  char weather_name[16]; //天气现象名称
  char weather_code[4];  //天气现象代码
  char temp[5];          //温度
  char last_update[25];  //最后更新时间
};
ActualWeather actual;  //创建结构体变量 目前的

struct FutureWeather
{
  char status_code[64]; //错误代码

  char date0[14];             //今天日期
  char date0_text_day[20];    //白天天气现象名称
  char date0_code_day[4];     //白天天气现象代码
  char date0_text_night[16];  //晚上天气现象名称
  char date0_code_night[4];   //晚上天气现象代码
  char date0_high[5];         //最高温度
  char date0_low[5];          //最低温度
  char date0_humidity[5];     //相对湿度

  char date1[14];             //明天日期
  char date1_text_day[20];    //白天天气现象名称
  char date1_code_day[4];     //白天天气现象代码
  char date1_text_night[16];  //晚上天气现象名称
  char date1_code_night[4];   //晚上天气现象代码
  char date1_high[5];         //最高温度
  char date1_low[5];          //最低温度
  char date1_humidity[5];     //相对湿度

  char date2[14];             //后天日期
  char date2_text_day[20];    //白天天气现象名称
  char date2_code_day[4];     //白天天气现象代码
  char date2_text_night[16];  //晚上天气现象名称
  char date2_code_night[4];   //晚上天气现象代码
  char date2_high[5];         //最高温度
  char date2_low[5];          //最低温度
  char date2_humidity[5];     //相对湿度
};
FutureWeather future;//创建结构体变量 未来

struct Nongli //农历,暂未使用
{
  //https://api.timelessq.com/time
  char status_code[64]; //错误代码
  char error[1];     // 是否出错
  char errmsg[64];  // 错误信息
};

struct Hitokoto  //一言API
{
  char status_code[64]; //错误代码
  char hitokoto[64];
  //https://v1.hitokoto.cn?encode=json&charset=utf-8&min_length=1&max_length=21
  //https://pa-1251215871.cos-website.ap-chengdu.myqcloud.com/
};
Hitokoto yiyan; //创建结构体变量 一言

//****** EEPROM地址和定义 ******
#define eepromVar_address 0 //起始地址
struct MyEEPROMStruct
{
  uint8_t auto_state;  //自动刷写eeprom状态 0-需要 1-不需要
  char  city[64];        //城市
  char  weatherKey[64];  //天气KEY
} eepromVar;
//****** 一些变量 ******
String webServer_news = " ";
uint8_t client_count = 0;  //连接服务器的超时计数,暂未使用
uint8_t client_error = 0;  //错误代码,暂未使用
boolean night_updated_state = 1; //夜间更新 1-不更新 0-更新
//RTC临时数据
#define RTC_hour_dz 0           //小时地址
#define RTC_night_count_dz 1    //夜间计数地址
#define RTC_peiwang_state_dz 2  //配网状态地址
uint32_t RTC_hour = 100;        //小时
uint32_t RTC_night_count = 0;   //24-6点，夜间不更新计数
int32_t night_count_max = 0;    //需要跳过几次
uint32_t RTC_peiwang_state = 0; //配网状态 1-需要
//****** 引脚配置 cn.ntp.org.cn
#define LED D4
int bright = LOW; //默认亮灯的电平
#define bat_switch_pin 12
#define bat_vcc_pin A0  //读取电池电压引脚
float bat_vcc = 0.0;    //电池电压
#define key_zj  0  //中间的按键
#define key_yb  5  //右边的按键

void setup()
{
  pinMode(key_zj, INPUT); //INPUT_PULLUP
  pinMode(key_yb, INPUT_PULLUP); //INPUT_PULLUP
  attachInterrupt(digitalPinToInterrupt(key_yb), int_key_yb, RISING);
  //一些初始化
  Serial.begin(74880);
  display.init();
  ESP.wdtEnable(60000);     //使能软件看门狗的触发间隔

  not_updated_at_night();   //夜间不更新

  display.setRotation(3);   //设置方向
  u8g2Fonts.begin(display); //将u8g2过程连接到Adafruit GFX
  timeClient.begin();
  
  //ESP.wdtFeed();//喂狗
  //Serial.println(css);


  ESP.rtcUserMemoryRead(RTC_peiwang_state_dz, &RTC_peiwang_state, sizeof(RTC_peiwang_state));
  Serial.print("读取 RTC_peiwang_state数据："); Serial.println(RTC_peiwang_state); Serial.println(" ");
  auto_eeprom();
  //是否进入配网模式
  peiwang_mod();

  connectToWifi();          //连接wifi
  //connectToWifi_Multi();  //连接wifi

  display_setup();   //显示连接WIFI界面
  GetData();         //获取数据界面
  display_main();    //显示主界面
  //display_tbcs();  //图标测试

  pinMode(key_yb, INPUT); //INPUT_PULLUP

  //查看堆碎片
  Serial.print("堆碎片度量："); Serial.println(ESP.getHeapFragmentation()); Serial.println(" ");

  esp_sleep(60);
  
  //ESP.wdtFeed();       //喂狗
}

void loop()
{
  /*if (digitalRead(5) == 0)
    {
    Serial.print("GPIO5"); Serial.println(" ");
    }
    else if (digitalRead(0) == 0)
    {
    Serial.print("GPIO0"); Serial.println(" ");
    }*/
}

ICACHE_RAM_ATTR void int_key_yb()
{
  //进入配网模式
  if (RTC_peiwang_state > 1)RTC_peiwang_state = 0;
  if (RTC_peiwang_state == 1) RTC_peiwang_state = 0;
  else if (RTC_peiwang_state == 0) RTC_peiwang_state = 1;
  ESP.rtcUserMemoryWrite(RTC_peiwang_state_dz, &RTC_peiwang_state, sizeof(RTC_peiwang_state));
  ESP.reset(); //ESP.reset();
}

/*
  ESP.rtcUserMemoryRead(0, &data, sizeof(data));//向地址0写入data
  Serial.println(" ");
  Serial.print("读取 TRC地址0数据："); Serial.println(data); Serial.println(" ");
  data = 1;
  ESP.rtcUserMemoryWrite(0, &data, sizeof(data));//向地址0写入data
  Serial.print("data写入："); Serial.println(data); Serial.println(" ");
  ESP.rtcUserMemoryRead(0, &data, sizeof(data));//向地址0写入data
  Serial.print("读取 RTC地址0数据："); Serial.println(data); Serial.println(" ");
  Serial.print("堆碎片度量："); Serial.println(ESP.getHeapFragmentation()); Serial.println(" ");
*/
