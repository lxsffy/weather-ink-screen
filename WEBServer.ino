/*** 初始化webserver配置 ***/
//server.handleClient(); //处理http请求
/*** 初始化webserver配置 ***/
void initWebServer()
{
  // 启动闪存文件系统
  if (SPIFFS.begin()) Serial.println("SPIFFS 启动成功");
  else                Serial.println("SPIFFS 未能成功启动");
  // 显示目录中文件内容以及文件大小
  Dir dir = SPIFFS.openDir("/");  // 建立“目录”对象
  while (dir.next())              // dir.next()用于检查目录中是否还有“下一个文件”
  {
    String fileName = dir.fileName(); //文件名
    size_t fileSize = dir.fileSize(); //文件大小
    Serial.printf("文件:%s,大小:%d\n", fileName.c_str(), fileSize);
  }
  Serial.println("文件列表输出完毕"); Serial.println(" ");

  server.on("/", HTTP_POST, handleRootPost);   //上传网页的设置参数到服务器
  server.on("/", handleRoot);                  //获取网页请求
  server.onNotFound(handleNotFound);           //404

  server.on("/weather", HTTP_POST, handleWeatherPost);

  server.on("/RESET", handleRESET);

  server.on("/Updata", handleUpdata);

  server.on("/FileUpdata1", HTTP_POST, respondOK, handleFileUpload2); //上传处理页面

  server.on("/FileUpdata1", handleFileUpload1); //上传选择页面

  server.on("/Updata", handleUpdata);

  server.on("/ReadSTA_name_ip", handleReadSTA_name_ip);  //ajax 请求STAwifi名称和ip地址
  server.on("/Read_city", handleRead_city);              //ajax 请求城市名称


  //配置http更新服务为为&server
  httpUpdater.setup(&server, "/update", "Jones", "3333");
  //启动WebServer
  server.begin();
}

//回复状态码 200 给客户端
void respondOK()
{
  server.send(200);
}

void handleNotFound() //设置处理404情况的函数'handleUserRequest'
{
  String path = server.uri();
  Serial.print("load url:");
  Serial.println(path);
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path))
  {
    if (SPIFFS.exists(pathWithGz)) path += ".gz";
    File file = SPIFFS.open(path, "r");
    if (file)
    {
      Serial.print("文件系统打开:"); Serial.print(path); Serial.println(" 成功"); Serial.println("  ");
      /*while (file.available()) {
        //读取文件输出
        Serial.write(file.read());
        }*/
    }
    else {
      Serial.print("文件系统打开:");
      Serial.print(path);
      Serial.println(" 失败"); Serial.println("  ");
    }
    //server.sendHeader("header Content-Encoding=gzip", "/");
    server.streamFile(file, contentType);
    file.close();
    return;
  }
  server.send(404, "text/plain", "404 Not Found");
}

/*** 处理根目录get请求 ***/
void handleRoot() //处理网站根目录"/"的访问请求
{
  String path = "/GCSBS.html";
  String contentType = getContentType(path);  // 获取文件类型
  if (SPIFFS.exists(path))  // 如果访问的文件可以在SPIFFS中找到
  {
    Serial.print(path); Serial.println(" 使用文件系统");
    File file = SPIFFS.open(path, "r");   // 则尝试打开该文件
    server.streamFile(file, contentType); // 并且将该文件返回给浏览器
    delay(100);
    file.close();                        // 并且关闭文件
  }
  else
  {
    Serial.print(path); Serial.println(" 使用本地文件");
    server.send(200, "text/html", sendHTML_main());
  }
  webServer_news = "有设备访问";
  display_bottom(webServer_news);
  return;
}


/*** 处理根目录post请求 ***/
void handleRootPost()
{
  String wifi_xiaoxi = "WIFI保存成功";
  //char *strcpy(char *dest, const char *src) 把 src 所指向的字符串复制到 dest。
  //arg(name) —— 根据请求key获取请求参数的值
  //arg(index) —— 获取第几个请求参数的值
  if (server.hasArg("ssid")) //查询是否存在某个参数
  {
    strcpy(sta_ssid, server.arg("ssid").c_str());  //保存参数
    if (strlen(sta_ssid) == 0) wifi_xiaoxi = "WIFI名称不能为空"; //检查WIFI名称长度为0时退出
  }
  else wifi_xiaoxi = "未找到参数'ssid'";

  //查询是否存在某个参数
  if (server.hasArg("password"))
    strcpy(sta_password, server.arg("password").c_str()); //保存参数
  else wifi_xiaoxi += "  未找到参数'password'";

  //server.send(200, "text/html", sendHTML_return_home(wifi_xiaoxi));
  server.send(200, "text/plain", wifi_xiaoxi);
  if (wifi_xiaoxi == "WIFI保存成功")
  {
    WiFi.disconnect();
    WiFi.begin(sta_ssid, sta_password);  //要连接的WiFi名称和密码
    WiFi.mode(WIFI_AP_STA); //设置工作模式
    sta_count = 0;
    delay(100);
  }
  display_bottom(wifi_xiaoxi);
}

/*** 处理天气相关post请求 ***/
boolean tqqc_state = 0; //天气信息清除
void handleWeatherPost()
{
  String web_message;
  //查询是否存在某个参数 WeatherKey
  if (server.hasArg("WeatherKey")) //查询是否存在某个参数
  {
    strcpy(eepromVar.weatherKey, server.arg("WeatherKey").c_str());
    if (strlen(eepromVar.weatherKey) != 0)  //检查名称长度为0时退出
    {
      EEPROM.put(eepromVar_address, eepromVar);
      EEPROM.commit(); //保存
      web_message = "天气KEY保存成功";
    }
    else
    {
      EEPROM.get(eepromVar_address, eepromVar);
      web_message = "天气Key不能为空";
    }
  }
  else
  {
    web_message = "WeatherKey 参数不存在";
  }

  if (server.hasArg("city")) //查询是否存在某个参数
  {
    strcpy(eepromVar.city, server.arg("city").c_str());
    if (strlen(eepromVar.city) != 0)        //检查名称长度为0时退出
    {
      EEPROM.put(eepromVar_address, eepromVar);
      EEPROM.commit(); //保存
      web_message += "<br>城市保存成功";
    }
    else
    {
      EEPROM.get(eepromVar_address, eepromVar);
      web_message += "<br>城市不能为空";
    }
  }
  else
  {
    web_message += "<br>city 参数不存在";
  }
  server.send(200, "text/plain", web_message);
  display_bottom(web_message);
  tqqc_state = 1;
}

// 选择上传文件页
void handleFileUpload1()
{
  server.send(200, "text/html", sendHTML_fileUpdata());
  display_bottom("文件上传页面");
}
// 处理上传文件函数
void handleFileUpload2() {

  HTTPUpload& upload = server.upload();

  if (upload.status == UPLOAD_FILE_START) // 如果上传状态为UPLOAD_FILE_START
  {
    String filename = upload.filename;                        // 建立字符串变量用于存放上传文件名
    if (!filename.startsWith("/")) filename = "/" + filename; // 为上传文件名前加上"/"
    Serial.println("文件名字: " + filename);                   // 通过串口监视器输出上传文件的名称

    fsUploadFile = SPIFFS.open(filename, "w");                // 在SPIFFS中建立文件用于写入用户上传的文件数据

  }
  else if (upload.status == UPLOAD_FILE_WRITE) // 如果上传状态为UPLOAD_FILE_WRITE
  {
    if (fsUploadFile) fsUploadFile.write(upload.buf, upload.currentSize); // 向SPIFFS文件写入浏览器发来的文件数据
  }
  else if (upload.status == UPLOAD_FILE_END) // 如果上传状态为UPLOAD_FILE_END
  {
    if (fsUploadFile) // 如果文件成功建立
    {
      Dir dir = SPIFFS.openDir("/");  // 建立“目录”对象
      while (dir.next())                   // dir.next()用于检查目录中是否还有“下一个文件”
      {
        String fileName = dir.fileName(); //文件名
        size_t fileSize = dir.fileSize(); //文件大小
        Serial.printf("文件:%s,大小:%d\n", fileName.c_str(), fileSize);
        /*Serial.print(dir.fileName());      // 输出文件名
          Serial.print(" 大小: ");            // 通过串口监视器输出文件大小
          Serial.println(upload.totalSize);*/
      }
      Serial.println("文件列表输出完毕");
      fsUploadFile.close();                    // 将文件关闭
      server.send(303, "text/html", sendHTML_return_home("文件上传成功"));
      //server.sendHeader("Location", sendHTML_return_home("文件上传成功")); // 将浏览器跳转到/success.html（成功上传页面）
      //server.send(303);                                       // 发送相应代码303（重定向到新页面）
      display_bottom("文件上传成功");
    }
    else // 如果文件未能成功建立
    {
      Serial.println("文件上传失败");                       // 通过串口监视器输出报错信息
      server.send(500, "text/html", sendHTML_return_home("500 无法上传文件"));
      display_bottom("500 文件上传失败");
    }
  }
}

void handleReadSTA_name_ip() //ajax请求STA名字和ip
{
  String web_message = WiFi.SSID() + " " + WiFi.localIP().toString();
  if (WiFi.localIP().toString() == "(IP unset)" && sta_count >= 10) web_message = WiFi.SSID() + " 连接失败";
  else if (WiFi.localIP().toString() == "(IP unset)" && sta_count < 10) web_message = WiFi.SSID() + " 连接中，勿进行其他操作";
  server.send(200, "text/plain", web_message);
}
void handleRead_city() //ajax请求天气
{
  String web_message = String(eepromVar.city) + " ";
  if (WiFi.isConnected() == 1 && tqqc_state == 0) web_message += String(actual.weather_name) + " " + String(actual.temp) + "℃";
  server.send(200, "text/plain", web_message);
}
void handleUpdata()
{
  server.send(200, "text/html", sendHTML_gujian_update());
  webServer_news = "Web更新界面";
  display_bottom(webServer_news);
}
//*** 系统重启
void handleRESET()
{
  webServer_news = "系统准备重启";
  display_bottom(webServer_news);
  String RESET;
  RESET = "<meta charset = 'UTF-8'>系统重启，该页面已失效";
  server.send(200, "text/html", RESET);
  if (webServer_news == "系统准备重启")
  {
    RTC_peiwang_state = 0;
    ESP.rtcUserMemoryWrite(RTC_peiwang_state_dz, &RTC_peiwang_state, sizeof(RTC_peiwang_state));
    //WiFi.begin(sta_ssid, sta_password);  //要连接的WiFi名称和密码
    delay(2000);
    ESP.reset();
  }
}

// 获取文件类型
String getContentType(String filename)
{
  if (filename.endsWith(".htm")) return "text/html";
  else if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".png")) return "image/png";
  else if (filename.endsWith(".gif")) return "image/gif";
  else if (filename.endsWith(".jpg")) return "image/jpeg";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".xml")) return "text/xml";
  else if (filename.endsWith(".pdf")) return "application/x-pdf";
  else if (filename.endsWith(".zip")) return "application/x-zip";
  else if (filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}
