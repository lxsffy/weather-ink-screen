/*** 初始化STA配置 ***/
uint32_t sta_count = 0;
boolean qqtq_state = 1; //请求天气状态位
void connectToWifi()
{
  //WiFi.disconnect();   //断开WIFI连接,清掉之前最近一个连接点信息
  WiFi.mode(WIFI_STA); //设置工作模式
  //delay(10);
  WiFi.begin();  //要连接的WiFi名称和密码
  //WiFi.begin();
}
void connectToWifi_Multi()
{
  // 将需要连接的一系列WiFi ID和密码输入这里,ESP8266-NodeMCU再启动后会扫描当前网络
  // 环境查找是否有这里列出的WiFi ID。如果有则尝试使用此处存储的密码进行连接。
  wifiMulti.addAP("K20", "326513988");
  wifiMulti.addAP("Jones", "326513988");
  wifiMulti.addAP("Tenda_2B4C90", "liwenjie1246753866");
}
/*** 初始化AP配置 ***/
void initApSTA()
{
  WiFi.mode(WIFI_AP_STA); //设置工作模式
  WiFi.softAPConfig(local_IP, gateway, subnet); //ap的网络参数
  WiFi.softAP(ap_ssid, ap_password, 1, 0, 1);  //ap的名称和密码
}
void peiwang_mod()
{
  if (RTC_peiwang_state == 1)
  {
    initApSTA();       //初始化ApSTA
    initWebServer();   //初始化WEB服务器、webServerOTA服务
    display_peiwang();
    uint32_t time_old = 0;
    String xiaoxi;
    while (RTC_peiwang_state == 1)
    {
      server.handleClient(); //处理http请求
      if (millis() - time_old > 1000) //每隔1S让出100MS来处理网络信息
      {

        //Serial.print("当前模式"); Serial.println(WiFi.getMode());
        //Serial.print("8266连接WIFI状况："); Serial.println(WiFi.isConnected());
        //Serial.print("webServer_news："); Serial.println(webServer_news);
        //Serial.print("8266连接WIFI状况："); Serial.println(WiFi.waitForConnectResult());

        //超过一定时间没有连接上WiFi就切换成ap模式
        if (WiFi.isConnected() == 0)
        {
          delay(100);
          if (sta_count < 10)
          {

            sta_count++;
            xiaoxi = String(sta_count);
            display_bottom2(WiFi.SSID() + " " + "连接中" + " " + xiaoxi); //发送消息到屏幕倒数第二行
          }
          if (sta_count >= 10 && xiaoxi != "8266未连接上WIFI")
          {
            xiaoxi = "8266未连接上WIFI";
            display_bottom2(xiaoxi + "  " + WiFi.SSID() + "  已启动热点"); //发送消息到屏幕倒数第二行
          }
          if (sta_count >= 10 && WiFi.getMode() != 2) WiFi.mode(WIFI_AP); //大于一定时间关闭STA模式
        }
        else if (WiFi.isConnected() == 1 && xiaoxi != "8266已连接WIFI")
        {
          sta_count = 0;
          xiaoxi = "8266已连接WIFI";
          display_bottom2(xiaoxi + "  " + WiFi.SSID() + "  " + WiFi.localIP().toString()); //发送消息到屏幕倒数第二行
        }
        if (WiFi.isConnected() == 1 && qqtq_state == 1)
        {
          WiFi.mode(WIFI_STA);
          //拼装实况天气API地址
          url_ActualWeather = "https://api.seniverse.com/v3/weather/now.json";
          url_ActualWeather += "?key=" + String(eepromVar.weatherKey);
          url_ActualWeather += "&location=" + String(eepromVar.city);
          url_ActualWeather += "&language=" + language;
          url_ActualWeather += "&unit=c";
          ParseActualWeather(callHttps(url_ActualWeather), &actual);
          WiFi.mode(WIFI_AP_STA);
          qqtq_state = 0;
        }

        time_old = millis();
      }

      //设定一些触发条件
      if (WiFi.softAPgetStationNum() == 0 && webServer_news != "没有设备连接")
      {
        webServer_news = "没有设备连接";
        display_bottom(webServer_news);
      }
    }
  }
}
/*
  WiFi.mode(); WIFI_AP /WIFI_STA /WIFI_AP_STA /WIFI_OFF
*/
//****** OTA设置并启动 ******
//ArduinoOTA.handle();
/*void initOTA()
  {
  ArduinoOTA.setHostname("esp8266");
  ArduinoOTA.setPassword("333333");
  ArduinoOTA.begin();
  }*/
