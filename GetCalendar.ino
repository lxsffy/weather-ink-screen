//日历数据计算公式  d为几号，m为月份，y为年份
String week_calculate(int y, int m, int d)
{
  if (m == 1 || m == 2)  //一二月换算
  {
    m += 12;
    y--;
  }
  int week = (d + 2 * m + 3 * (m + 1) / 5 + y + y / 4 - y / 100 + y / 400 + 1) % 7;
  if (week == 1) return"星期一";
  else if (week == 2 ) return"星期二";
  else if (week == 3 ) return"星期三";
  else if (week == 4 ) return"星期四";
  else if (week == 5 ) return"星期五";
  else if (week == 6 ) return"星期六";
  else if (week == 0 ) return"星期天";
  else return "计算出错";
  //其中1~6表示周一到周六 0表示星期天
}

/*//未加入闰年判断
  String week_calculate(String n, String y, String r)
  {
  int nian = atoi(n.c_str());
  int yue = atoi(y.c_str());
  int ri = atoi(r.c_str());
  if (nian <= 0 && nian >= 2199) return "n(年) 错误";
  else if (yue <= 0 && yue >= 12) return "y(月) 错误";
  else if (ri <= 0 && ri >= 31) return "r(日) 错误";
  if (yue == 1 || yue == 2)  //一二月换算
  {
    yue += 12;
    nian--;
  }
  int week = (ri + 2 * yue + 3 * (yue + 1) / 5 + nian + nian / 4 - nian / 100 + nian / 400 + 1) % 7;
  // char* week_c;
  // itoa(week, week_c, 10);
  // String week_s = week_c;
  // return week_s;
  // 其中1~6表示周一到周六 0表示星期天
  if (week == 1) return"星期一";
  else if (week == 2 ) return"星期二";
  else if (week == 3 ) return"星期三";
  else if (week == 4 ) return"星期四";
  else if (week == 5 ) return"星期五";
  else if (week == 6 ) return"星期六";
  else if (week == 0 ) return"星期天";
  else return "计算出错";
  }*/
