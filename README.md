![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V011-2.jpg)
### 注意事项
* 用开发板搭的，A0一定要接电池，不然会永久休眠，必须接分压电路，不能超过1V，否者会烧掉ADC
* GPIO5要下降沿才能触发，才能进入配网模式，不懂什么是下降沿自己百度
* 默认无wifi参数无KEY，需要自己到配网模式设置

### 介绍（请勿用于商业用途）
 **开放源码，但始终比最新版低一个版本<br>
时间功能正在添加，24H校准一次，1分钟更新一次然后休眠唤醒以节省电量，自动修正时间误差，误差一天不超过10秒。<br>
暂支持汉朔2.9寸墨水屏，丝印代号E029A01。<br>
如需其他同尺寸的屏幕，请留言<br>
使用Arduino开发，使用到的库GxEPD2、U8g2_for_Adafruit_GFX、NTPClient、ArduinoJson、ESP_EEPROM。<br>
使用心知天气个人免费版KEY（20次/分钟），需要自己去申请，然后在配网界面输入即可。<br>
提供3D打印外壳，裸面版、全包版本已发布<br>
硬件使用[DUCK的天气墨水屏项目](http://oshwhub.com/duck/esp8266-weather-station-epaper)，感觉串口不是很正常，谨慎使用。<br>
改良版硬件地址 [https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer](https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer) ，代码暂无适配温湿度传感器，可不焊接。<br>
观看视频 [https://www.bilibili.com/video/BV1up4y1h7ih](https://www.bilibili.com/video/BV1up4y1h7ih)<br>
配网模式：开机按下按键3即可进入（GPIO5下降沿触发）<br>** 
### 版本更新介绍
#### v011
* 添加紫外线强度指数、相对湿度、风力等级
* 加大实况温度字体，自动居中
* 配网页面增加电压显示类型切换按钮，电压原始值 或 百分比
* 配网页面增加更多的提示

#### v010
* 美化配网web界面，使用模态框代替页面跳转，看着舒服很多
* 配网web界面，加入关于按钮，显示系统版本和状态
* 配网web界面，加入更多设置按钮，包含以下功能:
* 加入夜间不更新功能开关功能，可在配网界面开启关闭
* 加入自定义一句话功能，可在配网界面设置
#### v009
* 不记得了

### 功能简介
* 实况天气和未来2日天气
* web配网功能
* webOTA功能
* 一言功能，即屏幕中间一句话，每次更新天气更新
* 夜间不更新功能，可在配网界面开启关闭
* 自定义一句话功能，可在配网界面设置
* GB2312一二级字库+全国城市字库，完全可应付日常使用
* 配网模式时，配网信息、web信息、电压信息显示
* 开机壁纸、载入数据显示
* 低电量提示并永久休眠，小于3.3V，日历图标处显示电池电压
* 休眠电流0.026ma，工作电流120-70ma

### 已知BUG
* 开机载入数据有小几率会重启系统，EXCCAUSE Code(3),加载或存储期间的处理器内部物理地址或数据错误？
* 配网界面更换城市时无法即时更新天气数据，https get数据失败，暂无能力解决。
* 在配网页面连接无效的的WIFI会卡一段时间，有相应提示。可能是硬件问题，无法同时进行STA和AP的收发？等待提示连接失败即可操作其他。
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V011-1.jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/IMG_20210314_120901.jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/%E9%85%8D%E7%BD%91%E7%95%8C%E9%9D%A21.png)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V011-3.jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/%E7%83%A7%E5%BD%95%E8%AF%B4%E6%98%8E.png)
