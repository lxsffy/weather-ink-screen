//首次开机自动刷写EEPROM
void auto_eeprom()
{
  //自动计算需要用多少eeprom数量
  EEPROM.begin(sizeof(MyEEPROMStruct));
  //获取eeprom数据
  EEPROM.get(eepromVar_address, eepromVar);
  if (eepromVar.auto_state != 1) eepromVar.auto_state = 0;
  if (eepromVar.auto_state == 0)
  {
    eepromVar.auto_state = 1;  //自动刷写eeprom状态 0-需要 1-不需要
    
    //为将要存储在EEPROM中的内容设置初始值(默认值)
    eepromVar.city[64] = {0};        //城市
    eepromVar.weatherKey[64] = {0};  //天气key

    EEPROM.put(eepromVar_address, eepromVar);
    EEPROM.commitReset(); //首次保存覆盖掉旧的数值
    Serial.println("EEPROM put");
  }
  if (eepromVar.auto_state == 1)
  {
    EEPROM.get(eepromVar_address, eepromVar);
    Serial.println("EEPROM get");
    //Serial.print("eepromVar.weatherKey:"); Serial.println(eepromVar.weatherKey);
    //Serial.print("eepromVar.city:"); Serial.println(eepromVar.city);
  }
}
