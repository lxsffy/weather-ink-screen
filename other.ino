//系统休眠
void esp_sleep(uint32_t minutes)
{
  digitalWrite(bat_switch_pin, 0); //关闭电池测量
  pinMode(bat_switch_pin, INPUT);  //改为输入状态避免漏电
  display.hibernate(); //屏幕进入深度睡眠
  if (minutes > 70) minutes = 70;
  else if (get_bat_vcc_nwe() < 3.0) minutes = 0;
  ESP.deepSleep(minutes * 60 * 1000000, WAKE_RF_DEFAULT);  //WAKE_RF_DEFAULT  WAKE_RFCAL  WAKE_NO_RFCAL  WAKE_RF_DISABLED
}
//获取数字的位数
uint8_t get_num_digit(uint32_t num)
{
  uint8_t count = 0;
  if (num == 0) count = 1;
  while (num != 0)
  {
    // n = n/10
    num /= 10;
    ++count;
  }
  return count;
}

void not_updated_at_night() //夜间不更新
{
  if (RTC_peiwang_state == 1)return;   //配网模式退出
  if (night_updated_state == 0)return; //夜间更新没打开退出
  ESP.rtcUserMemoryRead(RTC_hour_dz, &RTC_hour, sizeof(RTC_hour));
  Serial.print("读取 TRC地址0，RTC_hour："); Serial.println(RTC_hour); Serial.println(" ");
  if (RTC_hour >= 0 && RTC_hour <= 5)
  {
    ESP.rtcUserMemoryRead(RTC_night_count_dz, &RTC_night_count, sizeof(RTC_night_count));
    Serial.print("读取 TRC地址1，RTC_night_count："); Serial.println(RTC_night_count); Serial.println(" ");
    night_count_max = 5 - RTC_hour;
    if (RTC_night_count > 6) RTC_night_count = 0;
    if (RTC_night_count <= night_count_max)
    {
      RTC_night_count ++;
      ESP.rtcUserMemoryWrite(RTC_night_count_dz, &RTC_night_count, sizeof(RTC_night_count));
      esp_sleep(60);
    }
    else
    {
      RTC_night_count = 0;
      ESP.rtcUserMemoryWrite(RTC_night_count_dz, &RTC_night_count, sizeof(RTC_night_count));
    }
  }
}

/*使用ESP.restart()可以软复位系统；

  RTC存储区使用
  使用ESP.rtcUserMemoryWrite(offset, &data, sizeof(data))可以向RTC存储区写数据；
  使用ESP.rtcUserMemoryRead(offset, &data, sizeof(data))可以从RTC存储区读数据；
  RTC存储区共支持128个4字节的数据（即总共可存储512字节内容），地址offset取值为0 ~ 127；

  RTC存储区在系统复位时（非重新上电及固件上传）数据保持不变


  //启动WIFI省电功能
  //uint8_t i = WiFi.setSleepMode(WIFI_MODEM_SLEEP); //WIFI_LIGHT_SLEEP    WIFI_MODEM_SLEEP
  //Serial.print("省电模式："); Serial.println(WiFi.setSleepMode(WIFI_MODEM_SLEEP, 100)); Serial.println(" ");
*/
