//采集电池电压
//采用时间平均滤波
uint8_t bat_vcc_count = 0;      //采样计数
#define bat_vcc_cycs 10         //采样次数
#define bat_vcc_cysj 300        //采样时间
float bat_adc_new = 0.0;
uint32_t bat_vcc_time_old = 0.0;
void get_bat_vcc() //滤波的电压
{
  if (millis() - bat_vcc_time_old >= bat_vcc_cysj)
  {
    pinMode(bat_switch_pin, OUTPUT);
    digitalWrite(bat_switch_pin, 1);
    bat_vcc_count++;
    bat_adc_new += analogRead(bat_vcc_pin);
    if (bat_vcc_count >= bat_vcc_cycs)
    {
      bat_vcc = (bat_adc_new / bat_vcc_cycs) * 0.0009765625 * 5.537; //电池电压系数
      bat_adc_new = 0.0;
      bat_vcc_count = 0;
    }
    digitalWrite(bat_switch_pin, 0); //关闭电池测量
    pinMode(bat_switch_pin, INPUT);  //改为输入状态避免漏电
    bat_vcc_time_old = millis();
  }
}

float get_bat_vcc_nwe() //即时的电压
{
  pinMode(bat_switch_pin, OUTPUT);
  digitalWrite(bat_switch_pin, 1);
  float vcc_cache = 0.0;
  for (uint8_t i = 0; i < 10; i++)
  {
    delay(1);
    vcc_cache += analogRead(bat_vcc_pin) * 0.0009765625 * 5.537;
  }
  digitalWrite(bat_switch_pin, 0); //关闭电池测量
  pinMode(bat_switch_pin, INPUT);  //改为输入状态避免漏电
  return (vcc_cache / 10);
}
