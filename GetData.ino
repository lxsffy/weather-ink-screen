void GetData()
{
  //"http://api.seniverse.com/v3/weather/now.json?key=S6pG_Q54kjfnBAi6i&location=深圳&language=zh-Hans&unit=c"
  //拼装实况天气API地址
  url_ActualWeather = "https://api.seniverse.com/v3/weather/now.json";
  url_ActualWeather += "?key=" + String(eepromVar.weatherKey);
  url_ActualWeather += "&location=" + String(eepromVar.city);
  url_ActualWeather += "&language=" + language;
  url_ActualWeather += "&unit=c";
  //https://api.seniverse.com/v3/weather/daily.json?key=S6pG_Q54kjfnBAi6i&location=深圳&language=zh-Hans&unit=c&start=0&days=3
  //拼装实况未来API地址
  url_FutureWeather = "https://api.seniverse.com/v3/weather/daily.json";
  url_FutureWeather += "?key=" + String(eepromVar.weatherKey);
  url_FutureWeather += "&location=" + String(eepromVar.city);
  url_FutureWeather += "&language=" + language;
  url_FutureWeather += "&unit=c";
  url_FutureWeather += "&start=0";
  url_FutureWeather += "&days=3";
  //请求数据并Json处理
  display_bitmap_bottom(Bitmap_wlq1, "获取实况天气数据中");
  ParseActualWeather(callHttps(url_ActualWeather), &actual);

  display_bitmap_bottom(Bitmap_wlq2, "获取未来天气数据中");
  ParseFutureWeather(callHttps(url_FutureWeather), &future);

  display_bitmap_bottom(Bitmap_wlq3, "获取一言数据中");
  ParseHitokoto(callHttps(url_yiyan), &yiyan);

  display_bitmap_bottom(Bitmap_wlq4, "获取时间");
  //获取时间
  uint8_t update_count = 0;
  while (timeClient.update() == 0 && update_count <= 10)
  {
    update_count++;
    delay(100);
    Serial.print("NTP超时计数："); Serial.println(update_count);
  }
  if (update_count <= 10)
  {
    RTC_hour = timeClient.getHours();
    ESP.rtcUserMemoryWrite(RTC_hour_dz, &RTC_hour, sizeof(RTC_hour));
    //Serial.println(timeClient.getFormattedTime());
    timeClient.end();
    display_bottom("     OK!     ", 0); delay(500);
  }
  else
  {
    String a; String b;
    a = actual.last_update[11]; b = actual.last_update[12]; //String转char
    RTC_hour = atoi(a.c_str()) * 10; //char转int
    RTC_hour += atoi(b.c_str()) + 1;
    ESP.rtcUserMemoryWrite(RTC_hour_dz, &RTC_hour, sizeof(RTC_hour));
    Serial.print("RTC_hour:"); Serial.println(RTC_hour);
    display_bottom("获取ntp时间失败,改用天气时间", 0); delay(500);
  }
}
