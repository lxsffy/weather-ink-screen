//连接wifi的显示界面
uint8_t wifi_count = 0;
boolean wifi_cs = 0;  //wifi连接超时 0-无 1-超时
#define wifi_csyz 30  //wifi超时阈值
#define cg_y1 112
#define cg_y2 cg_y1+14
#define cg_x1 40

void display_setup()
{
  u8g2Fonts.setFontMode(1);          // 使用u8g2透明模式（这是默认设置）
  u8g2Fonts.setFontDirection(0);     // 从左到右（这是默认设置）
  u8g2Fonts.setForegroundColor(heise);  // 设置前景色
  u8g2Fonts.setBackgroundColor(baise);  // 设置背景色
  ESP.wdtFeed();//喂狗
  u8g2Fonts.setFont(chinese_city_gb2312);
  display_Bitmap_Setup(); //开机壁纸
  delay(1200);
  display_ljwifi();       //连接WIFI字样

  while (!WiFi.isConnected())
  {
    wifi_count++;
    display_jdthdy();    //显示进度条和电压
    Serial.println("正在连接WIFI");
  }
  /*while (wifiMulti.run() != WL_CONNECTED) // 尝试进行wifi连接。
    {
    wifi_count++;
    display_jdthdy();    //显示进度条和电压
    Serial.println("正在连接WIFI");
    }*/

  display_ljcg(); //连接成功 wifi 显示ip地址

  Serial.println(WiFi.localIP().toString());
  Serial.println(" ");

}

void display_Bitmap_Setup() //显示开机图片
{
  display.setPartialWindow(0, 0, 296, 128); //设置局部刷新窗口
  display.firstPage();
  do
  {
    display.drawInvertedBitmap(0, 0, Bitmap_xztq, 296, 128, heise); //显示开机图片
  }
  while (display.nextPage());
}

const char* zi_fu1 = "正在连接WIFI- ";

void display_ljwifi() //显示 正在连接WIFI 的字
{
  display.setPartialWindow(0, cg_y1 , display.width(), 16); //设置局部刷新窗口
  display.firstPage();
  do
  {
    u8g2Fonts.setCursor(cg_x1, cg_y2);
    u8g2Fonts.print(zi_fu1);
    u8g2Fonts.setCursor(cg_x1 + u8g2Fonts.getUTF8Width(zi_fu1), cg_y2);
    u8g2Fonts.print(WiFi.SSID());
  }
  while (display.nextPage());
  const char* updatemode;
  if (display.epd2.hasFastPartialUpdate)
  {
    updatemode = "快速局部刷新";
  }
  else if (display.epd2.hasPartialUpdate)
  {
    updatemode = "缓慢局部刷新";
  }
  else
  {
    updatemode = "没有局部刷新";
  }
  Serial.print("显示模式:"); Serial.println(updatemode);

}

void display_jdthdy() //显示 >>> 的进度条 和电压
{
  double batVcc = get_bat_vcc_nwe();
  const char* zhifu2 = "电池:";

  //float 转换成 char*
  char* zhifu3 = new char[4];
  gcvt(batVcc, 3, zhifu3);
  //Serial.println(zhifu3);
  uint16_t x2 = cg_x1 + u8g2Fonts.getUTF8Width(zi_fu1) + u8g2Fonts.getUTF8Width(WiFi.SSID().c_str()) + 6; //计算电池电压X位置
  uint16_t x3 = x2 + u8g2Fonts.getUTF8Width(zhifu2) + 3;
  uint16_t x4 = x3 + u8g2Fonts.getUTF8Width(zhifu3) + 2;
  display.firstPage();
  do
  {
    display.setPartialWindow(0, cg_y1, cg_x1 - 2, 16);

    if (wifi_count >= wifi_csyz) wifi_cs = 1;
    if (wifi_cs)
    {
      u8g2Fonts.setCursor(0, cg_y2);
      u8g2Fonts.print("超时！");
    }
    else
    {
      uint16_t x1 = wifi_count % 5 * 8; //计算进度条X位置
      u8g2Fonts.setCursor(x1, cg_y2 - 2);
      u8g2Fonts.print(">");
    }
  }
  while (display.nextPage());

  display.firstPage();
  do
  {
    display.setPartialWindow(x2, cg_y1, display.width() - x2, 16);

    u8g2Fonts.setCursor(x2, cg_y2);
    u8g2Fonts.print(zhifu2);
    u8g2Fonts.setCursor(x3, cg_y2);
    u8g2Fonts.print(zhifu3);
    u8g2Fonts.setCursor(x4, cg_y2);
    u8g2Fonts.print("V");

    //显示超时值
    //u8g2Fonts.setCursor(275, cg_y2);
    //u8g2Fonts.print(wifi_count);
  }
  while (display.nextPage());

  if (wifi_cs) esp_sleep(70);
}

void display_ljcg() //显示 ok ip地址
{
  String z = "OK！" + WiFi.localIP().toString();
  const char* zhifu4 = z.c_str(); //String转换char
  uint16_t zf_width = u8g2Fonts.getUTF8Width(zhifu4); /*+ 8g2Fonts.getUTF8Width(WiFi.localIP());*/
  uint16_t x = (display.width() / 2) - (zf_width / 2);

  //Serial.print("ip地址："); Serial.println(zhifu4);
  //Serial.print("X位置："); Serial.println(x);

  display.setPartialWindow(0, cg_y1, display.width(), 16);
  display.firstPage();
  do
  {
    display.fillScreen(baise);     // 填充屏幕
    u8g2Fonts.setCursor(x, cg_y2);
    u8g2Fonts.print(zhifu4);
  }
  while (display.nextPage());
}

void display_bottom(const char* zhifu5, boolean width_state = 1) //发送显示信息到屏幕底部 ,处理char型
{
  uint16_t zf_width = u8g2Fonts.getUTF8Width(zhifu5);  //获取字符的长度
  uint16_t x = (display.width() / 2) - (zf_width / 2); //计算字符居中的X位置
  if (width_state == 0)display.setPartialWindow(x, cg_y1, zf_width, 16); //设置局部显示区域
  else display.setPartialWindow(0, cg_y1, display.width(), 16);       //设置局部显示区域
  display.firstPage();
  do
  {
    u8g2Fonts.setCursor(x, cg_y2);
    u8g2Fonts.print(zhifu5);
  }
  while (display.nextPage());
}

void display_bottom(String z, boolean width_state = 1) //发送显示信息到屏幕底部,处理String型
{
  const char* zhifu6 = z.c_str();                      //String转换char
  uint16_t zf_width = u8g2Fonts.getUTF8Width(zhifu6);  //获取字符的长度
  uint16_t x = (display.width() / 2) - (zf_width / 2); //计算字符居中的X位置
  if (width_state == 0)display.setPartialWindow(x, cg_y1, zf_width, 16); //设置局部显示区域
  else display.setPartialWindow(0, cg_y1, display.width(), 16);       //设置局部显示区域
  display.firstPage();
  do
  {
    u8g2Fonts.setCursor(x, cg_y2);
    u8g2Fonts.print(zhifu6);
  }
  while (display.nextPage());
}

void display_bitmap_bottom(const unsigned char* bitmaps, String z) //显示开机图片,并在底部显示文字
{
  const char* zhifu6 = z.c_str();                      //String转char
  uint16_t zf_width = u8g2Fonts.getUTF8Width(zhifu6);  //获取字符的长度
  uint16_t x = (display.width() / 2) - (zf_width / 2); //计算字符居中的X位置
  display.setPartialWindow(0, 0, 296, 128); //设置局部刷新窗口
  display.firstPage();
  do
  {
    display.drawInvertedBitmap(0, 0, bitmaps, 296, 128, heise); //显示开机图片
    u8g2Fonts.setCursor(x, cg_y2);
    u8g2Fonts.print(zhifu6);
  }
  while (display.nextPage());
}

void display_bottom2(String z, boolean width_state = 1) //发送消息到底部倒数第二行，局部更新
{
  const char* zhifu6 = z.c_str();                      //String转换char
  uint16_t zf_width = u8g2Fonts.getUTF8Width(zhifu6);  //获取字符的长度
  uint16_t x = (display.width() / 2) - (zf_width / 2); //计算字符居中的X位置
  if (width_state == 0) display.setPartialWindow(x, cg_y1 - 16, zf_width, 16); //设置局部显示区域
  else display.setPartialWindow(0, cg_y1 - 16, display.width(), 16);     //设置局部显示区域
  display.firstPage();
  do
  {
    u8g2Fonts.setCursor(x, cg_y2 - 16);
    u8g2Fonts.print(zhifu6);
  }
  while (display.nextPage());
}
/*void display_Bitmap() //显示开机图片
  {
  display.setPartialWindow(0, 0, 296, 120); //设置局部刷新窗口
  display.fillScreen(baise);
  display.drawInvertedBitmap(0, 0, Bitmap_setup1, 296, 128, heise); //显示开机图片
  display.display(1); // full update
  }*/
//setPartialWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
//根据实际旋转使用参数。
//x和w应该是8的倍数，对于旋转0或2，
//y和h应该是8的倍数，对于旋转1或3，
//否则窗口会根据需要增加，
//这是电子纸控制器的寻址限制
