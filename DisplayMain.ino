//3天情况的位置
#define day_x0 55
#define day_x1 day_x0+29
#define day_x2 day_x1+38
#define day_x3 day_x2+46
#define day_y0 90
#define day_y1 day_y0+18
#define day_y2 day_y1+18
//小提示图标位置
#define cg_y0 14
#define cg_y1 cg_y0+19
#define cg_y2 cg_y1+19
#define cg_x0  1
//实况温度位置
uint16_t temp_x;
#define temp_y0 48
//圈圈位置
uint16_t circle_x;

void display_main()
{
  display.setFullWindow(); //设置全屏刷新 height
  display.firstPage();
  do
  {
    //display.setPartialWindow(0, 0, display.width(), display.height()); //设置局部刷新窗口

    //display.fillScreen(heise);  // 填充屏幕
    //display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲
    //display.fillScreen(baise);  // 填充屏幕
    //display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲

    /* u8g2Fonts.setCursor(275, 16); //实时时间-小时
      u8g2Fonts.print(RTC_hour);
      u8g2Fonts.setCursor(275, 32); //实时时间-小时
      u8g2Fonts.print(RTC_night_count);*/

    //提取最后更新时间的 时分
    String at;
    for (uint8_t i = 11; i < 16; i++)
    {
      at += actual.last_update[i];
    }

    display.drawInvertedBitmap(cg_x0, cg_y0 - 12, Bitmap_gengxing, 13, 13, heise); //画最后更新时间小图标
    u8g2Fonts.setCursor(cg_x0 + 15, cg_y0); //最后更新时间
    u8g2Fonts.print(at);

    display.drawInvertedBitmap(cg_x0, cg_y1 - 12, Bitmap_weizhi, 13, 13, heise); //画位置小图标
    u8g2Fonts.setCursor(cg_x0 + 15, cg_y1); //城市名
    u8g2Fonts.print(actual.city);

    display.drawInvertedBitmap(cg_x0, cg_y2 - 12, Bitmap_zhuangtai, 13, 13, heise); //画天气状态小图标
    u8g2Fonts.setCursor(cg_x0 + 15, cg_y2); //实况天气状态
    u8g2Fonts.print(actual.weather_name);

    //提取月日字符串 格式02-02
    String day0, day1, day2;
    for (uint8_t i = 5; i < 10; i++)
    {
      day0 += future.date0[i];
      day1 += future.date1[i];
      day2 += future.date2[i];
    }

    //显示一言数据
    uint16_t yiyan_width = u8g2Fonts.getUTF8Width(yiyan.hitokoto);  //获取字符的长度
    uint16_t yiyan_x = (display.width() / 2) - (yiyan_width / 2); //计算字符居中的X位置

    //display.drawLine(yiyan_x - 2, 56, yiyan_x + yiyan_width + 2, 56, 0); //画水平线
    //display.drawLine(yiyan_x - 2, 72, yiyan_x + yiyan_width + 2, 72, 0); //画水平线

    display.drawLine(0, 56, 295, 56, 0); //画水平线
    display.drawLine(0, 72, 295, 72, 0); //画水平线


    u8g2Fonts.setCursor(yiyan_x, 70);
    u8g2Fonts.print(yiyan.hitokoto);
    //显示3天的日期 月日
    u8g2Fonts.setCursor(day_x0, day_y0);
    u8g2Fonts.print("今天");
    u8g2Fonts.setCursor(day_x0, day_y1);
    u8g2Fonts.print("明天");
    u8g2Fonts.setCursor(day_x0, day_y2);
    u8g2Fonts.print("后天");

    u8g2Fonts.setCursor(day_x1, day_y0);
    u8g2Fonts.print(day0);
    u8g2Fonts.setCursor(day_x1, day_y1);
    u8g2Fonts.print(day1);
    u8g2Fonts.setCursor(day_x1, day_y2);
    u8g2Fonts.print(day2);

    //显示星期几
    //提取年月日并转换成int
    String nian0, nian1, nian2, yue0, yue1, yue2, ri0, ri1, ri2;
    int nian0_i, nian1_i, nian2_i, yue0_i, yue1_i, yue2_i, ri0_i, ri1_i, ri2_i;
    for (uint8_t i = 0; i <= 9; i++)
    {
      if (i <= 3)
      {
        nian0 += future.date0[i];
        nian1 += future.date1[i];
        nian2 += future.date2[i];
      }
      else if (i == 5 || i == 6)
      {
        yue0 += future.date0[i];
        yue1 += future.date1[i];
        yue2 += future.date2[i];
      }
      else if (i == 8 || i == 9)
      {
        ri0 += future.date0[i];
        ri1 += future.date1[i];
        ri2 += future.date2[i];
      }
    }
    nian0_i = atoi(nian0.c_str()); yue0_i = atoi(yue0.c_str()); ri0_i = atoi(ri0.c_str());
    nian1_i = atoi(nian1.c_str()); yue1_i = atoi(yue1.c_str()); ri1_i = atoi(ri1.c_str());
    nian2_i = atoi(nian2.c_str()); yue2_i = atoi(yue2.c_str()); ri2_i = atoi(ri2.c_str());

    //Serial.print("年:"); Serial.print(nian0_i); Serial.print(" "); Serial.print(nian1_i); Serial.print(" "); Serial.println(nian2_i);
    //Serial.print("月:"); Serial.print(yue0_i); Serial.print(" "); Serial.print(yue1_i); Serial.print(" "); Serial.println(yue2_i);
    //Serial.print("日:"); Serial.print(ri0_i); Serial.print(" "); Serial.print(ri1_i); Serial.print(" "); Serial.println(ri2_i);
    //Serial.println(week_calculate(nian, yue, ri));

    u8g2Fonts.setCursor(day_x2, day_y0);
    u8g2Fonts.print(week_calculate(nian0_i, yue0_i, ri0_i));
    u8g2Fonts.setCursor(day_x2, day_y1);
    u8g2Fonts.print(week_calculate(nian1_i, yue1_i, ri1_i));
    u8g2Fonts.setCursor(day_x2, day_y2);
    u8g2Fonts.print(week_calculate(nian2_i, yue2_i, ri2_i));

    //显示白天和晚上的天气现象
    uint16_t x_zl = 5; //高低温X位置增量
    String text_day0, text_night0, dn0_s;
    String text_day1, text_night1, dn1_s;
    String text_day2, text_night2, dn2_s;
    const char* dn0; const char* dn1; const char* dn2;

    if (strcmp(future.date0_text_day, future.date0_text_night) != 0) //今天
    {
      text_day0 = future.date0_text_day;
      text_night0 = future.date0_text_night;
      dn0_s = text_day0 + "转" + text_night0;
      dn0 = dn0_s.c_str();
      u8g2Fonts.setCursor(day_x3, day_y0);
      u8g2Fonts.print(dn0);
    }
    else
    {
      dn0 = future.date0_text_night;
      u8g2Fonts.setCursor(day_x3, day_y0);
      u8g2Fonts.print(dn0);
    }

    if (strcmp(future.date1_text_day, future.date1_text_night) != 0) //明天
    {
      text_day1 = future.date1_text_day;
      text_night1 = future.date1_text_night;
      dn1_s = text_day1 + "转" + text_night1;
      dn1 = dn1_s.c_str();
      u8g2Fonts.setCursor(day_x3, day_y1);
      u8g2Fonts.print(dn1);
    }
    else
    {
      dn1 = future.date1_text_night;
      u8g2Fonts.setCursor(day_x3, day_y1);
      u8g2Fonts.print(dn1);
    }

    if (strcmp(future.date2_text_day, future.date2_text_night) != 0) //后天
    {
      text_day2 = future.date2_text_day;
      text_night2 = future.date2_text_night;
      dn2_s = text_day2 + "转" + text_night2;
      dn2 = dn2_s.c_str();
      u8g2Fonts.setCursor(day_x3, day_y2);
      u8g2Fonts.print(dn2);
    }
    else
    {
      dn2 = future.date2_text_night;
      u8g2Fonts.setCursor(day_x3, day_y2);
      u8g2Fonts.print(dn2);
    }

    //显示高低温
    String  high0, high1, high2, low0, low1, low2, hl0_s, hl1_s, hl2_s;
    high0 = future.date0_high; high1 = future.date1_high; high2 = future.date2_high;
    low0 = future.date0_low; low1 = future.date1_low; low2 = future.date2_low;
    hl0_s = high0 + "/" + low0;
    hl1_s = high1 + "/" + low1;
    hl2_s = high2 + "/" + low2;
    const char* hl0 = hl0_s.c_str();
    const char* hl1 = hl1_s.c_str();
    const char* hl2 = hl2_s.c_str();
    u8g2Fonts.setCursor(day_x3 + u8g2Fonts.getUTF8Width(dn0) + x_zl, day_y0);
    u8g2Fonts.print(hl0);
    u8g2Fonts.setCursor(day_x3 + u8g2Fonts.getUTF8Width(dn1) + x_zl, day_y1);
    u8g2Fonts.print(hl1);
    u8g2Fonts.setCursor(day_x3 + u8g2Fonts.getUTF8Width(dn2) + x_zl, day_y2);
    u8g2Fonts.print(hl2);


    //显示实况温度，对比城市和天气现象的字符长度，来决定温度的位置
    uint16_t city_width = u8g2Fonts.getUTF8Width(actual.city); //获取城市的字符长度
    uint16_t weather_name_width = u8g2Fonts.getUTF8Width(actual.weather_name); //获取天气现象的字符长度
    if (city_width >= weather_name_width) temp_x = u8g2Fonts.getUTF8Width(actual.city) + 30;
    else temp_x = u8g2Fonts.getUTF8Width(actual.weather_name) + 30;

    //char *temp = "-58";
    //strcpy(actual.temp,temp);
    u8g2Fonts.setFont(u8g2_font_logisoso38_tn);
    u8g2Fonts.setCursor(temp_x, temp_y0); //显示温度
    u8g2Fonts.print(actual.temp);

    circle_x = temp_x + u8g2Fonts.getUTF8Width(actual.temp) + 7; //计算圈圈的位置

    //Serial.print("getUTF8Width(actual.temp):"); Serial.println(u8g2Fonts.getUTF8Width(actual.temp));

    display.drawCircle(circle_x, temp_y0 - 33, 3, 0); //画圆圈
    display.drawCircle(circle_x, temp_y0 - 33, 4, 0);
    //显示天气图标
    display_tbpd();
    //显示日历图标
    display.drawInvertedBitmap(1, 77, Bitmap_riqi, 50, 50, heise);
    //显示电压
    float batvcc = get_bat_vcc_nwe();
    u8g2Fonts.setFont(u8g2_font_blipfest_07_tn );
    u8g2Fonts.setCursor(19, 94);
    u8g2Fonts.print(batvcc);
    //低压提示
    if (batvcc <= 3.3) display.drawInvertedBitmap(271, 92, Bitmap_batlow, 24, 35, heise);
  }
  while (display.nextPage());
  //display.display(1);
}

void display_tbpd() //天气图标显示
{
  uint16_t x = circle_x + 20; //计算图标的位置
  uint16_t y = 5;
  String code, hour_s;
  int hour;
  hour = actual.last_update[12];
  hour += actual.last_update[13];
  hour = atoi(hour_s.c_str());
  code = actual.weather_code;
  if (code == "0")      display.drawInvertedBitmap(x, y + 5, Bitmap_qt, 45, 45, heise);
  else if (code == "1") display.drawInvertedBitmap(x, y, Bitmap_qt_ws, 45, 45, heise);
  else if (code == "2") display.drawInvertedBitmap(x, y + 5, Bitmap_qt, 45, 45, heise);
  else if (code == "3") display.drawInvertedBitmap(x, y, Bitmap_qt_ws, 45, 45, heise);
  else if (code == "4") display.drawInvertedBitmap(x, y, Bitmap_dy, 45, 45, heise);
  else if (code == "5") display.drawInvertedBitmap(x, y, Bitmap_dy, 45, 45, heise);
  else if (code == "6") display.drawInvertedBitmap(x, y, Bitmap_dy_ws, 45, 45, heise);
  else if (code == "7") display.drawInvertedBitmap(x, y, Bitmap_dy, 45, 45, heise);
  else if (code == "8") display.drawInvertedBitmap(x, y, Bitmap_dy_ws, 45, 45, heise);
  else if (code == "9") display.drawInvertedBitmap(x, y + 3, Bitmap_yt, 45, 45, heise);
  else if (code == "10") display.drawInvertedBitmap(x, y, Bitmap_zheny, 45, 45, heise);
  else if (code == "11") display.drawInvertedBitmap(x, y, Bitmap_lzy, 45, 45, heise);
  else if (code == "12") display.drawInvertedBitmap(x, y, Bitmap_lzybbb, 45, 45, heise);
  else if (code == "13") display.drawInvertedBitmap(x, y, Bitmap_xy, 45, 45, heise);
  else if (code == "14") display.drawInvertedBitmap(x, y, Bitmap_zhongy, 45, 45, heise);
  else if (code == "15") display.drawInvertedBitmap(x, y, Bitmap_dayu, 45, 45, heise);
  else if (code == "16") display.drawInvertedBitmap(x, y, Bitmap_by, 45, 45, heise);
  else if (code == "17") display.drawInvertedBitmap(x, y, Bitmap_dby, 45, 45, heise);
  else if (code == "18") display.drawInvertedBitmap(x, y, Bitmap_tdby, 45, 45, heise);
  else if (code == "19") display.drawInvertedBitmap(x, y, Bitmap_dongy, 45, 45, heise);
  else if (code == "20") display.drawInvertedBitmap(x, y, Bitmap_yjx, 45, 45, heise);
  else if (code == "21") display.drawInvertedBitmap(x, y, Bitmap_zhenx, 45, 45, heise);
  else if (code == "22") display.drawInvertedBitmap(x, y, Bitmap_xx, 45, 45, heise);
  else if (code == "23") display.drawInvertedBitmap(x, y, Bitmap_zhongy, 45, 45, heise);
  else if (code == "24") display.drawInvertedBitmap(x, y, Bitmap_dx, 45, 45, heise);
  else if (code == "25") display.drawInvertedBitmap(x, y, Bitmap_bx, 45, 45, heise);
  else if (code == "26") display.drawInvertedBitmap(x, y, Bitmap_fc, 45, 45, heise);
  else if (code == "27") display.drawInvertedBitmap(x, y, Bitmap_ys, 45, 45, heise);
  else if (code == "28") display.drawInvertedBitmap(x, y, Bitmap_scb, 45, 45, heise);
  else if (code == "29") display.drawInvertedBitmap(x, y, Bitmap_scb, 45, 45, heise);
  else if (code == "30") display.drawInvertedBitmap(x, y+5, Bitmap_w, 45, 45, heise);
  else if (code == "31") display.drawInvertedBitmap(x, y, Bitmap_m, 45, 45, heise);
  else if (code == "32") display.drawInvertedBitmap(x, y, Bitmap_f, 45, 45, heise);
  else if (code == "33") display.drawInvertedBitmap(x, y, Bitmap_f, 45, 45, heise);
  else if (code == "34") display.drawInvertedBitmap(x, y, Bitmap_jf, 45, 45, heise);
  else if (code == "35") display.drawInvertedBitmap(x, y, Bitmap_rdfb, 45, 45, heise);
  //else if (code == 37) display.drawInvertedBitmap(x,y, Bitmap_dy, 45, 45, heise);
  //else if (code == 38) display.drawInvertedBitmap(x,y, Bitmap_dy, 45, 45, heise);
  else if (code == "99") display.drawInvertedBitmap(x, y, Bitmap_wz, 45, 45, heise);
}
void display_tbcs() //图标测试
{
  display.fillScreen(baise);  // 填充屏幕
  display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲

  display.drawInvertedBitmap(0, 0, Bitmap_qt, 45, 45, heise);
  display.drawInvertedBitmap(50, 0, Bitmap_dy, 45, 45, heise);
  display.drawInvertedBitmap(100, 0, Bitmap_yt, 45, 45, heise);
  display.drawInvertedBitmap(150, 0, Bitmap_zheny, 45, 45, heise);
  display.drawInvertedBitmap(200, 0, Bitmap_lzybbb, 45, 45, heise);
  display.drawInvertedBitmap(250, 0, Bitmap_xy, 45, 45, heise);

  display.drawInvertedBitmap(0, 40, Bitmap_zhongy, 45, 45, heise);
  display.drawInvertedBitmap(50, 40, Bitmap_dayu, 45, 45, heise);
  display.drawInvertedBitmap(100, 40, Bitmap_by, 45, 45, heise);
  display.drawInvertedBitmap(150, 40, Bitmap_dby, 45, 45, heise);
  display.drawInvertedBitmap(200, 40, Bitmap_tdby, 45, 45, heise);
  display.drawInvertedBitmap(250, 40, Bitmap_dongy, 45, 45, heise);

  display.drawInvertedBitmap(0, 80, Bitmap_yjx, 45, 45, heise);
  display.drawInvertedBitmap(50, 80, Bitmap_zhenx, 45, 45, heise);
  display.drawInvertedBitmap(100, 80, Bitmap_xx, 45, 45, heise);
  display.drawInvertedBitmap(150, 80, Bitmap_zhongx, 45, 45, heise);
  display.drawInvertedBitmap(200, 80, Bitmap_dx, 45, 45, heise);
  display.drawInvertedBitmap(250, 80, Bitmap_bx, 45, 45, heise);
  display.display(1);
  delay(5000);

  display.fillScreen(baise);  // 填充屏幕
  display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲

  display.drawInvertedBitmap(0, 0, Bitmap_fc, 45, 45, heise);
  display.drawInvertedBitmap(50, 0, Bitmap_ys, 45, 45, heise);
  display.drawInvertedBitmap(100, 0, Bitmap_scb, 45, 45, heise);
  display.drawInvertedBitmap(150, 0, Bitmap_w, 45, 45, heise);
  display.drawInvertedBitmap(200, 0, Bitmap_m, 45, 45, heise);
  display.drawInvertedBitmap(250, 0, Bitmap_f, 45, 45, heise);

  display.drawInvertedBitmap(0, 40, Bitmap_jf, 45, 45, heise);
  display.drawInvertedBitmap(50, 40, Bitmap_rdfb, 45, 45, heise);
  display.drawInvertedBitmap(100, 40, Bitmap_ljf, 45, 45, heise);
  display.drawInvertedBitmap(150, 40, Bitmap_wz, 45, 45, heise);
  display.drawInvertedBitmap(200, 40, Bitmap_qt_ws, 45, 45, heise);
  display.drawInvertedBitmap(250, 40, Bitmap_yt_ws, 45, 45, heise);

  display.drawInvertedBitmap(0, 80, Bitmap_dy_ws, 45, 45, heise);
  display.drawInvertedBitmap(50, 80, Bitmap_zy_ws, 45, 45, heise);
  display.drawInvertedBitmap(100, 80, Bitmap_zx_ws, 45, 45, heise);

  display.display(1);
}
